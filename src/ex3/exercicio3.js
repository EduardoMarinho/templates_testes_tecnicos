// Resolução
let nivel = prompt("Qual o nível de água?")

if (nivel > 1000) {
    alert("Reservatório está com defeito. Realizar manutenção!")
} else if (nivel >= 200 && nivel <= 1000) {
    alert("Nível do reservatório estável.")
} else {
    alert("Abastecer o reservatório.")
}