// Resolução
let bateriaRestante = prompt("Qual o número restante de bateria?")

if (bateriaRestante < 0 || bateriaRestante > 100) {
    alert("Valor inválido.")
} else if (bateriaRestante >= 20 && bateriaRestante <= 100) {
    alert("Não é preciso recarregar no momento.")
} else {
    alert("Recarregue seu telefone.")
}